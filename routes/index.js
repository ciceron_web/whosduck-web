var express = require('express');
var router = express.Router();
var request = require('request');
var urlencode = require('urlencode');

/* GET home page. */
router.get('/', function (req, res, next) {
	res.redirect('/fanfics');
	//   viewFile(req, res, 'index');
	// res.render('index', { title: 'Express' });
});

router.get('/fanfics', function (req, res, next) {
	viewFile(req, res, 'fanfics/fanfics-main');
});

router.get('/fanfics/tags', function (req, res, next) {
	viewFile(req, res, 'fanfics/fanfics-tags');
});

router.get('/fanfics/view/:id/:id2', function (req, res, next) {
	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		// url: req.protocol + '://secure.c.i' + '/api/user/profile',
		headers: req.headers
	}, function (err, res2, body) {
		// console.log(body);
		try {

			body = JSON.parse(body);

			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
				var previews = trans.resource_info.filter(resource => resource.preview_permission == 0 || resource.preview_permission == 2).map(function (file) {
					file.file_url = file.file_url.split('/').pop();
					return file;
				});
				viewFile(req, res, 'fanfics/view/fanfics-viewer', { file: { name: trans.theme, description: trans.description, date: new Date(trans.register_timestamp).toLocaleDateString(), time: new Date(trans.register_timestamp).toUTCString(), project_id: req.params.id, resource_id: trans.id, language_id: trans.target_language_id, preview_count: previews.length, cover: body.cover_photo_url, previewFileList: previews, author: body.author, source: body.quote_url } });
			});
			return;
		}
		catch (e) {
			next();
		}
	});
});



router.get('/market', function (req, res, next) {
	viewFile(req, res, 'market/market-main');
});



var viewFile = function (req, res, filename, param) {
	var fullUrl = req.protocol + '://' + req.get('host') + req.path;

	res.render("" + filename, { title: 'CICERON', filename: filename.split("/").pop(), user: null, url: fullUrl, encodedUrl: urlencode(fullUrl), req: req, param });

	
	// request({
	// 	url: req.protocol + '://' + req.get('host') + '/api/v2/user/profile',
	// 	// url: req.protocol + '://secure.c.i' + '/api/user/profile',
	// 	headers: req.headers
	// }, function (err, res2, body) {
	// 	// console.log(body);

	// 	try {
	// 		// console.log(JSON.parse(body));
	// 		res.render("" + filename, { title: 'CICERON', filename: filename.split("/").pop(), user: JSON.parse(body), url: fullUrl, encodedUrl: urlencode(fullUrl), req: req, param });

	// 	} catch (error) {
	// 		res.render("" + filename, { title: 'CICERON', filename: filename.split("/").pop(), user: null, url: fullUrl, encodedUrl: urlencode(fullUrl), req: req, param });
	// 	}
	// });
	//   // console.log(req);
	// console.log(req.headers);
}


module.exports = router;
