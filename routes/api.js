var express = require('express');
var router = express.Router();
var request = require('request');
var urlencode = require('urlencode');

/* GET users listing. */
router.get('/a', function (req, res, next) {
    res.send('respond with a resource');
});

router.get('/fanfics/getLatestFanfics', function (req, res, next) {
    request({
        url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/platform/whosduck?lang_id=2',
        headers: req.headers
    }, function (err, res2, body) {
        try {
            body = JSON.parse(body);
            body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
                var f = trans.resource_info.filter(resource => resource.id == req.params.page && (resource.preview_permission == 0));
                request({
                    url: req.protocol + '://' + req.get('host') + f[0].file_url,
                    headers: req.headers
                }).pipe(res);
            });
            return;

        } catch (error) {
            next();
        }
    });
});

router.get('/file/:id/lang/:id2/preview/:page', function (req, res, next) {
	request({
		url: req.protocol + '://' + req.get('host') + '/api/v2/user/pretranslated/project/' + req.params.id + '/resource',
		headers: req.headers
	}, function (err, res2, body) {
		try {
			body = JSON.parse(body);
			body.data.filter(trans => trans.id == req.params.id2).map(function (trans) {
				var f = trans.resource_info.filter(resource => resource.id == req.params.page && (resource.preview_permission == 0));
				request({
					url: req.protocol + '://' + req.get('host') + f[0].file_url,
					headers: req.headers
				}).pipe(res);
			});
			return;

		} catch (error) {
			next();
		}
	});
});

module.exports = router;
