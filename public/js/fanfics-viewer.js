'use strict'

var module = function () {

	var copyTextToClipboard = function (text) {
		var textArea = document.createElement("textarea");

		textArea.style.position = 'fixed';
		textArea.style.top = 0;
		textArea.style.left = 0;
		textArea.style.width = '2em';
		textArea.style.height = '2em';
		textArea.style.padding = 0;
		textArea.style.border = 'none';
		textArea.style.outline = 'none';
		textArea.style.boxShadow = 'none';
		textArea.style.background = 'transparent';
		textArea.value = text;
		document.body.appendChild(textArea);
		textArea.select();
		var successful = false;
		try {
			successful = document.execCommand('copy');
		} catch (err) {
		}
		// console.log(successful);
		document.body.removeChild(textArea);
		return successful;
	}

	var getFileType = function (file) {
		return ['png', 'jpg', 'jpeg', 'gif', 'bmp'].indexOf(file.file_url.split(".").pop()) > -1 ? 'image' : 'html';
	};
	var getRenderingHtmlCodeForImage = function (file) {
		return '<img src="/api/file/' + project_id + '/lang/' + resource_id + '/preview/' + file.id + '">';
	};
	var getRenderingHtml = function (file) {
		$.ajax({
			url: '/api/file/' + project_id + '/lang/' + resource_id + '/preview/' + file.id,
			processData: false,
			contentType: false,
			dataType: "html",
			cache: false,
			type: 'GET'
		}).done(function (data) {
			data = data.indexOf("<br") > -1 ? data : data.split("\n").join("<br />");
			$('#section' + file.id).html(data);
		}).fail(function (xhr, ajaxOptions, thrownError) {
			console.log('section' + file.id + ' error');
		});
		return '<div id="section' + file.id + '" class="content-preview-section"></div>';
	};

	var getLastetTranslations = function () {
		var html = $(".content-latest").html();
		$(".content-latest").addClass('loading');
		$(".content-latest-item-cover").css('background-image', '');
		$(".content-latest-item-title").text('　　　　　　　　');
		console.log(html);
		$.ajax({
			url: '/translated/' + project_id + '/curation/' + language_id,
			processData: false,
			contentType: false,
			dataType: "json",
			cache: false,
			type: 'GET'
		}).done(function (data) {
			$(".content-latest").html(data.map(function (item) {
				console.log(item);
				return html.replace('{Title}', item.theme).replace('{Cover}', item.cover).replace('{Summary}', item.description).replace('{Project}', item.project_id).replace('{Resource}', item.resource_id);
			}).join(''));
			$(".content-latest").removeClass('loading');
		}).fail(function (xhr, ajaxOptions, thrownError) {
			// console.log('section' + file.id + ' error');
		}).always(function () {
		});
	}

	var renderContents = function () {
		$(".content-preview").html(previewFileList.map(function (file) {
			var val = getFileType(file) == 'image' ? getRenderingHtmlCodeForImage(file) : getRenderingHtml(file);
			return val;
		}).join(''));
	}

	$("body").on("click", ".share-facebook", function (e) {
		window.open("https://www.facebook.com/sharer/sharer.php?u=" + encodedUrl + "&display=popup&ref=plugin&src=share_button");
	});

	$("body").on("click", ".share-twitter", function (e) {
		window.open("https://twitter.com/intent/tweet?url=" + encodedUrl);
	});

	$("body").on('click', '.share-link', function () {
		if (copyTextToClipboard((location.origin).concat(location.pathname))) {
			// $(this).addClass('success');
			// setTimeout(function () {
			//     $('.share-link').removeClass('success');
			// }, 1000);
			alert("URL이 복사되었습니다.");
		}
		else {
			alert("URL 복사에 실패하였습니다.");

			// alert("");
		}
	});



	renderContents();
	getLastetTranslations();

	return;
};



$(document).ready(module());